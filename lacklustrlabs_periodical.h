#pragma once

#include <stdint.h>
#if defined(ARDUINO) && ARDUINO >=100
  #include <Arduino.h>
#else
  #include <WProgram.h>
#endif


namespace lacklustrlabs {

class Periodical {
  public:
    Periodical(uint32_t period): _period(period) {
      begin();
    }

    void begin() {
      _next = millis() + _period;
    }

    void begin(uint32_t period) {
      _period = period;
      begin();
    }

    bool isDue() {
      uint32_t now = millis();
      bool rv = false;

      while (_next <= now ) {
        _next += _period;
        rv = true;
      }
      return rv;
    }

    virtual bool operator()() {
      return isDue();
    }

  protected: 
    uint32_t _next;
    uint32_t _period;
};

}
