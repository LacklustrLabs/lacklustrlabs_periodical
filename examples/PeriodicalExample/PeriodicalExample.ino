#include <lacklustrlabs_periodical.h>

#if defined(ARDUINO_ARCH_STM32F1)
#define SOUT Serial1
#else
#define SOUT Serial
#endif

using namespace lacklustrlabs;


struct AnotherPeriodical:lacklustrlabs::Periodical {
  AnotherPeriodical(uint32_t period):Periodical(period){
  }

  virtual bool operator ()() {
    if (isDue()) {
      SOUT.println(F("periodicalB triggered"));
      return true;
    }
    return false;
  }
};

lacklustrlabs::Periodical periodicalA(1000);
AnotherPeriodical periodicalB(1200);

void setup() {
  SOUT.begin(115200);
  SOUT.println(F("Starting Periodical "));
  periodicalA.begin();
  periodicalB.begin();
}

void loop() { 
  if ( periodicalA() ) {
    SOUT.println(F("periodicalA triggered"));
  }

  periodicalB();
}

